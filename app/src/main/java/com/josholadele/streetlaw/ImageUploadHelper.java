package com.josholadele.streetlaw;

/**
 * Created by josh on 23/03/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.io.File;
import java.lang.reflect.Field;

public class ImageUploadHelper {

    public static final int CAMERA_CAPTURE = 1;
    public static final int IMAGE_GALLERY = 2;
    public static final int PIC_CROP = 3;

    Activity mContext;

    public ImageUploadHelper(Activity context) {
        this.mContext = context;
    }

    public void selectImage() {

        String[] options = {"From Camera", "From Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(true);
        builder.setTitle("Please Select Profile Picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    launchCameraActivity();
                } else if (which == 1) {
                    launchGalleryActivity(IMAGE_GALLERY);
                }
            }
        });
        builder.show();
    }


    public void launchCameraActivity() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "slaw-temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        mContext.startActivityForResult(intent, CAMERA_CAPTURE);
    }

    public void launchGalleryActivity(int requestCode) {
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        mContext.startActivityForResult(Intent.createChooser(pickIntent, "Select Picture"), requestCode);
    }

    public static Uri getImageURIFromCameraUpload() {

        File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/slaw-temp.jpg");
        if (f.exists()) {
            return Uri.fromFile(f);
        }
        Log.e("URL_FILE", Uri.fromFile(f).toString());
        return null;
    }

    public Bitmap getBitmapFromUri(Uri uri) {
        File f = new File(uri.getPath());
        Bitmap bitmap;
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
        return bitmap;
    }


}