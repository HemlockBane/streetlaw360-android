package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.ChooseJudgeActivity;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.Judge;
import com.josholadele.streetlaw.model.User;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by josh on 20/03/2018.
 */

public class AssigneesAdapter extends RecyclerView.Adapter<AssigneesAdapter.AssigneesViewHolder> {

    private List<User> theList;
    private Context context;
    private AssigneeClickListener assigneeClickListener;
    HashMap<Integer, CaseFile> selectionList;
    public HashSet<Integer> selectedPositions = new HashSet<>();

//    ChooseJudgeActivity activity;

    public boolean IS_MULTISELECT = false;

    public AssigneesAdapter(AssigneeClickListener clickListener) {
        this.assigneeClickListener = clickListener;
    }


    @Override
    public AssigneesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
//        activity = (ChooseJudgeActivity) context;
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assignee_item_view, parent, false);
        return new AssigneesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AssigneesViewHolder holder, int position) {

        User user = theList.get(position);
        holder.assigneeName.setText(user.displayName);
//        holder.assigneeImage;

    }

    public void setData(List<User> users) {
        this.theList = users;
        notifyDataSetChanged();
    }


    public void removeLawyer(User user) {
        if (theList.contains(user)) {
            theList.remove(user);
            notifyDataSetChanged();
        }
    }

    public List<User> getData() {
        return theList;
    }

    public void addSingleData(User user) {
        this.theList.add(user);// = caseFiles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }


    public interface AssigneeClickListener {
        void onAssigneeClick(User user, boolean isLongClick);
    }

    class AssigneesViewHolder extends RecyclerView.ViewHolder {

        TextView assigneeName;
        ImageView assigneeImage;


        public AssigneesViewHolder(final View itemView) {
            super(itemView);
            assigneeName = itemView.findViewById(R.id.assignee_name);
            assigneeImage = itemView.findViewById(R.id.assignee_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    assigneeClickListener.onAssigneeClick(theList.get(getAdapterPosition()), false);
                }
            });
        }
    }
}




