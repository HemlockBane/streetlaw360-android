package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.NotifyLawyerActivity;
import com.josholadele.streetlaw.model.Lawyer;

import java.util.HashSet;
import java.util.List;

/**
 * Created by josh on 20/03/2018.
 */

public class LawyerAdapter extends RecyclerView.Adapter<LawyerAdapter.LawyersViewHolder> {

    private List<Lawyer> theList;
    private Context context;
    private LawyerClickListener lawyerClickListener;
    public HashSet<Integer> selectedPositions = new HashSet<>();

    NotifyLawyerActivity activity;

    public boolean IS_MULTISELECT = false;

    public LawyerAdapter(LawyerClickListener clickListener) {
        this.lawyerClickListener = clickListener;
    }


    @Override
    public LawyersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        activity = (NotifyLawyerActivity) context;
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lawyer_item_view, parent, false);
        return new LawyersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LawyersViewHolder holder, int position) {

        Lawyer lawyer = theList.get(position);
        holder.judgeRole.setText(lawyer.email);
        holder.lawyerName.setText(lawyer.name);
        String initial = String.valueOf(lawyer.name.charAt(0));
        holder.judgeInitial.setText(initial);
//        holder.newsImage.setImageResource(R.drawable.ic_hammer);


//        holder.itemView.setActivated(selectedPositions.contains(position));

    }

    public void setLawyersData(List<Lawyer> judges) {
        this.theList = judges;
        notifyDataSetChanged();
    }

    public List<Lawyer> getData() {
        return theList;
    }

    public void addLawyer(Lawyer judge) {
        this.theList.add(judge);// = caseFiles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }

    public void clearSelections() {
//        for (int i : selectedPositions) {
//
//        }
        selectedPositions.clear();
        activity.updateToolbar(false);
    }

    public void deactivate() {
//        for (int i : selectedPositions) {
//
//            theList.get(i).
//        }
    }

    public interface LawyerClickListener {
        void onLawyerClick(Lawyer judge, boolean isLongClick);
    }

    class LawyersViewHolder extends RecyclerView.ViewHolder {

        //        ImageView newsImage;
        TextView lawyerName;
        TextView judgeInitial;

//        View lastView;


        TextView judgeRole;
        ImageView isSelected;
//        TextView genreText;

        public LawyersViewHolder(final View itemView) {
            super(itemView);
            lawyerName = itemView.findViewById(R.id.judge_name);
            judgeInitial = itemView.findViewById(R.id.judge_initial);
            isSelected = itemView.findViewById(R.id.is_selected);
//            newsImage = itemView.findViewById(R.id.img_case_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (selectedPositions.contains(getAdapterPosition())) {
                        selectedPositions.remove(getAdapterPosition());
                        isSelected.setVisibility(View.GONE);
                        itemView.setActivated(false);
                    } else {
                        selectedPositions.add(getAdapterPosition());
                        isSelected.setVisibility(View.VISIBLE);
                        itemView.setActivated(true);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (selectedPositions.contains(getAdapterPosition())) {
                        isSelected.setVisibility(View.GONE);
                        selectedPositions.remove(getAdapterPosition());
                        itemView.setActivated(false);
                    } else {
                        selectedPositions.add(getAdapterPosition());
                        isSelected.setVisibility(View.VISIBLE);
                        itemView.setActivated(true);

                    }
                    return true;
//                    return false;
                }
            });

            judgeRole = (TextView) itemView.findViewById(R.id.judge_role);
//            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
//            genreText = (TextView) itemView.findViewById(R.id.genre_text);

        }
    }
}




