package com.josholadele.streetlaw.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.NoticesAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.Notice;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.NoticeService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NoticesFragment extends Fragment implements NoticesAdapter.NoticeClickListener {


    RecyclerView noticesRecycler;
    NoticesAdapter mAdapter;
    TextView emptyViewText;
    AppSharedPref _sharedPref;
    ProgressBar contentLoading;
    View rootView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_notices, null);

        _sharedPref = new AppSharedPref(getContext());

        noticesRecycler = rootView.findViewById(R.id.notices_recyclerview);
        contentLoading = rootView.findViewById(R.id.progress_layout);
        emptyViewText = rootView.findViewById(R.id.empty_view);
        setupRecyclerView(noticesRecycler);

//        initGUI();

        return rootView;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new NoticesAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
//        getSampleNotices();
    }

    @Override
    public void onResume() {
        super.onResume();

        fetchNotices(_sharedPref.getUserId());
    }

//    List<Notice> getSampleNotices() {
//        List<Notice> files = new ArrayList<>();
//        Notice caseFile = new Notice(1, CaseStatus.AdoptionOfWrittenAddresses.toString(), "Aminu vs Tamiu");
//        Notice caseFile2 = new Notice(1, CaseStatus.Assigned.toString(), "Calos vs Rami");
//        Notice caseFile3 = new Notice(1, CaseStatus.Closed.toString(), "Ederamo vs Arafat");
//        Notice caseFile4 = new Notice(1, CaseStatus.Judgement.toString(), "Johnson vs Sol");
//        files.add(caseFile);
//        files.add(caseFile2);
//        files.add(caseFile3);
//        files.add(caseFile4);
//        files.add(caseFile);
//        mAdapter.setNoticeData(files);
//        return files;
//    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                getActivity().finish();
            }
            break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void onNoticeClick(Notice caseFile, boolean isLongClick) {

    }

    void fetchNotices(String userId) {

        showLoading();
        NoticeService noticeService = new NoticeService();
        int userIdInt = TextUtils.isEmpty(userId) ? 0 : Integer.parseInt(userId);
        noticeService.getByUser(userIdInt, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                List<Notice> caseFileList = buildFromResult(response.optJSONArray("Data"));
                if (caseFileList != null && caseFileList.size() > 0) {
                    showDataView();
                    mAdapter.setNoticeData(caseFileList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }


    private void showErrorMessage() {
        emptyViewText.setVisibility(View.VISIBLE);
        noticesRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showDataView() {
        emptyViewText.setVisibility(View.GONE);
        noticesRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyViewText.setVisibility(View.GONE);
        noticesRecycler.setVisibility(View.INVISIBLE);
        contentLoading.setVisibility(View.VISIBLE);
//        addCaseLayout.setVisibility(View.GONE);
    }


    private List<Notice> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONArray objectArray = null;
        try {
            objectArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<Notice> notices = new ArrayList<>();


        for (int i = 0; i < objectArray.length(); i++) {
            JSONObject object = objectArray.optJSONObject(i);

            List<User> associatedUsers = new ArrayList<>();
            JSONArray userArray = object.optJSONArray("AssociatedUsers");

            if (userArray != null && userArray.length() > 0) {
                for (int j = 0; j < userArray.length(); j++) {
                    try {
                        JSONObject userObject = userArray.getJSONObject(j);
                        User user = new User(userObject.optInt("UserId"), userObject.optString("FirstName") + " " + userObject.optString("LastName"),
                                userObject.optString("Username"), userObject.optString("Email"), userObject.optString("ProfilePicture"));
                        associatedUsers.add(user);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

//            notices.add(new Notice(object.optInt("Id"),
//                    object.optString("Message"),
//                    object.optString("Title")));

            // Notice(int id, String title, String message, String dateSent, List<User> users)
            Notice notice = new Notice(object.optInt("Id"), object.optString("Title"), object.optString("Message"), object.optString("DateSent"), associatedUsers);
            notices.add(notice);
        }
        return notices;
    }
}