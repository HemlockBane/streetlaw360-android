package com.josholadele.streetlaw.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.NewsDetailsActivity;
import com.josholadele.streetlaw.adapters.NewsAdapter;
import com.josholadele.streetlaw.model.News;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.NewsService;
import com.josholadele.streetlaw.utils.UserUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends Fragment implements NewsAdapter.NewsClickListener {

    View rootView = null;

    RecyclerView noticesRecycler;
    NewsAdapter mAdapter;
    ProgressBar contentLoading;
    TextView emptyView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_news, null);


        noticesRecycler = rootView.findViewById(R.id.news_recyclerView);
        emptyView = rootView.findViewById(R.id.empty_view);
        contentLoading = rootView.findViewById(R.id.progress_layout);
        setupRecyclerView(noticesRecycler);

        return rootView;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new NewsAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
        fetchNews(UserUtils.getCategory(getContext()));
    }

    void fetchNews(String userCategory) {

        showLoading();
        NewsService newsService = new NewsService();
        newsService.getNewsByCategory(userCategory, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                List<News> newsList = buildFromResult(response.optJSONArray("Data"));
                if (newsList != null && newsList.size() > 0) {
                    showDataView();
                    mAdapter.setNewsData(newsList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        noticesRecycler.setVisibility(View.INVISIBLE);
        contentLoading.setVisibility(View.GONE);
//        doCategoryCheck();

    }

    private void showDataView() {
        emptyView.setVisibility(View.GONE);
        noticesRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
//        doCategoryCheck();
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        noticesRecycler.setVisibility(View.INVISIBLE);
        contentLoading.setVisibility(View.VISIBLE);
    }


    private List<News> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONObject movieJSONObject;
        JSONArray movieJSONArray = null;
        try {
//                movieJSONObject = new JSONObject(result);
//                if (isItem) {
//                    movieJSONArray = movieJSONObject.optJSONArray("items");
//                } else {
//                    movieJSONArray = movieJSONObject.optJSONArray("results");
//                }
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<News> newsList = new ArrayList<>();


        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);

            News news = new News(object.optInt("Id"), object.optString("Story"), object.optString("HeadLine"), object.optString("PictureFileName"), object.optString("SourceUrl"));
/*
* object.optInt("Id"),
                    CaseStatus.valueOf(object.optString("Status")),
                    object.optString("Title")*/

            newsList.add(news);
        }
        return newsList;
    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                getActivity().finish();
            }
            break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onNewsClick(News news, boolean isLongClick) {
        Intent intent = new Intent(this.getContext(), NewsDetailsActivity.class);
        intent.putExtra("news", news);
        startActivity(intent);
    }
}