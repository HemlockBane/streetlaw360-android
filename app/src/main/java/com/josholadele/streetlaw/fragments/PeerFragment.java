package com.josholadele.streetlaw.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v8.renderscript.BaseObj;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonRequest;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.ChatScreenActivity;
import com.josholadele.streetlaw.adapters.ChatContactsAdapter;
import com.josholadele.streetlaw.adapters.ContactsListAdapter;
import com.josholadele.streetlaw.adapters.ConversationListAdapter;
import com.josholadele.streetlaw.adapters.PeerAdapter;
import com.josholadele.streetlaw.adapters.UserContactsAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.dummy.DummyContent;
import com.josholadele.streetlaw.dummy.DummyContent.DummyItem;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.modelBridge.ConversationBridge;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.TimeZone;
import java.util.Locale;
import java.io.IOException;
import java.util.Date;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PeerFragment extends Fragment implements ContactsListAdapter.ContactClickListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    RecyclerView contactsRecycler;
    ContactsListAdapter mAdapter;
    Toolbar mToolbar;

    TextView toolbarTitle;
    TextView toolbarSelectionText;

    TextView addCaseTV;
    ImageView addCaseImg;


    ProgressBar contentLoading;
    TextView emptyView;

    MenuItem assignMenu;
    MenuItem filterMenu;

    AppSharedPref _sharedPref;
    ArrayList<Integer> selectedCaseIds;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PeerFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PeerFragment newInstance(int columnCount) {
        PeerFragment fragment = new PeerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.peer_fragment_page, container, false);
        // fragment_peer_item_list
        contentLoading = view.findViewById(R.id.progress_layout);
        emptyView = view.findViewById(R.id.empty_view);

        contactsRecycler = view.findViewById(R.id.contact_recyclerview);

        setupRecyclerView(contactsRecycler);
        _sharedPref = new AppSharedPref(getContext());
        fetchContacts();
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new PeerAdapter(DummyContent.ITEMS, mListener));
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchContacts();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }

    @Override
    public void onContactClick(User user, boolean isLongClick) {

        Intent intent = new Intent(getActivity(), ChatScreenActivity.class);
        ChatContact selectedContact = new ChatContact(0, user.displayName, user.judicialDivision, user.userId, user.username, user.email, user.isLawyer);
        intent.putExtra("selected-contact", selectedContact);

        startActivity(intent);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new ContactsListAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);
        /*
        mAdapter = new UserContactsAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter); */
//        getSampleCaseFiles();
    }

    List<CaseFile> files;

    void fetchContacts() {

        showLoading();
        /*ConversationBridge conversationBridge = new ConversationBridge(getActivity());
        List<User> users = conversationBridge.getAllContacts();
        if (users != null && users.size() > 0) {
            showMovieDataView();
            mAdapter.setUsersData(users);
        } else {
            showErrorMessage();
        } */

        UserService userService = new UserService();
        userService.getUsersMessageState( Integer.valueOf(_sharedPref.getUserId()), new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {

                List<User> judgeList = buildFromResult(response.optJSONArray("Data"));
                //Log.e("ChatContact", judgeList.toString());
                if (judgeList != null && judgeList.size() > 0) {
                    showMovieDataView();
                    mAdapter.setUsersData(judgeList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.GONE);

    }

    private void showMovieDataView() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.VISIBLE);
    }

    private List<User> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONArray movieJSONArray = null;
        try {
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<ChatContact> judges = new ArrayList<>();
        List<User> dusers = new ArrayList<>();

        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);
            //String name = object.optString("FirstName") + " " + object.optString("LastName");
            //String category = object.optString("Category");
            JSONObject sender = object.optJSONObject("User");
            int uid = sender.optInt("UserId");
            String displayName = sender.optString("DisplayName");
            String profilePic = sender.optString("ProfilePictureUrl");
            String category = sender.optString("Category");
            String email = sender.optString("Email");
            String username = sender.optString("UserName");
            String lmessage = object.optString("Message");
            String time = object.optString("TimeSent");
            long ptime = parseDate(time);
            Log.e("WhatTime", time);

            User user = new User(uid, displayName, "", username, uid, email, profilePic, ptime, lmessage, true);
//            Judge judge = new Judge(object.optInt("Id"), name,
//                    object.optString("JudicialDivision"),
//                    object.optString("UserName"),
//                    object.optString("Email"));
            //ChatContact chatContact = new ChatContact(0, name, "", object.optInt("Id"), object.optString("Username"), object.optString("Email"), category.equalsIgnoreCase("lawyer"));
            dusers.add(user);
        }
        return dusers;
    }

    private long parseDate(String text)
            //throws ParseException
    {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",
                    Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Log.e("TestDate", dateFormat.parse(text).getTime() + "|" + text);
            return dateFormat.parse(text).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

        /*long startDate = 0;
        try {
            String dateString = text;
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a MM/dd/yyyy");
            Date date = sdf.parse(text);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate; */
    }

    public void onJudgeClick(final ChatContact selectedContact, boolean isLongClick) {

        ChatScreenActivity.launch(getActivity(), selectedContact);
        //getActivity().finish();
    }

    public void updateToolbar(boolean is_multiselect) {
        if (is_multiselect) {
            toolbarSelectionText.setVisibility(View.VISIBLE);
            String selectedNumer = mAdapter.selectedPositions.size() == 1 ? mAdapter.selectedPositions.size() + " case selected" : mAdapter.selectedPositions.size() + " cases selected";
            toolbarTitle.setVisibility(View.GONE);
            toolbarSelectionText.setText(selectedNumer);
            assignMenu.setVisible(true);
            filterMenu.setVisible(false);
        } else {
            toolbarSelectionText.setText("");
            toolbarSelectionText.setVisibility(View.GONE);
            toolbarTitle.setVisibility(View.VISIBLE);
            assignMenu.setVisible(false);
            filterMenu.setVisible(true);
        }
    }
}
