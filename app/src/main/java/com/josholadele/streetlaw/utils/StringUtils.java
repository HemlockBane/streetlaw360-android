package com.josholadele.streetlaw.utils;

/**
 * Created by josh on 31/03/2018.
 */

public class StringUtils {

    public static String capitalize(String word) {
        word = word.toLowerCase();
        word = word.substring(0, 1).toUpperCase() + word.substring(1);
        return word;
    }
}
