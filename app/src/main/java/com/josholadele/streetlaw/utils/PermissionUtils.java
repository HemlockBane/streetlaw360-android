package com.josholadele.streetlaw.utils;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;


/**
 * Created by Oladele on 10/25/17.
 */

public class PermissionUtils {

    public static final int PERMISSION_REQUEST_CODE = 23423;
    public static boolean checkPermissions(AppCompatActivity activity, @NonNull String[] permissions) {
        if (Build.VERSION.SDK_INT >= 23) {
            boolean allPermitted = true;
            ArrayList<String> strings = new ArrayList<>();
            for (String permission : permissions) {
                if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    allPermitted = false;
                    strings.add(permission);
                }
            }
            if (!allPermitted) {
                ActivityCompat.requestPermissions(activity, strings.toArray(new String[]{}), PERMISSION_REQUEST_CODE);
            }
            return allPermitted;
        } else {
            return true;
        }
    }

    public static boolean isWritePermissionGranted(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            return true;
        }
    }

    public static boolean isCameraPermissionGranted(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            return true;
        }
    }

    public static boolean isReadPermissionGranted(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            return true;
        }
    }
}
