package com.josholadele.streetlaw.utils;

import android.widget.Filter;

import com.josholadele.streetlaw.adapters.UserContactsAdapter;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;

import java.util.ArrayList;
import java.util.List;

public class ContactsFilter extends Filter {
    private final UserContactsAdapter mAdapter;
    List<ChatContact> contactList;
    List<ChatContact> filteredList;

    public ContactsFilter (List<ChatContact> contactList, UserContactsAdapter mAdapter) {
        this.mAdapter = mAdapter;
        this.contactList=contactList;
        this.filteredList=new ArrayList<>();
    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (constraint.length() == 0) {
            filteredList.addAll(contactList);
        } else {
            final String filterPattern = constraint.toString().toLowerCase().trim();

            for (final ChatContact user : contactList) {
                if (user.getName().contains(filterPattern) || user.getEmail().contains(filterPattern)) {
                    filteredList.add(user);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
       mAdapter.filteredList.clear();
       mAdapter.filteredList.addAll((ArrayList<ChatContact>) results.values);
       mAdapter.notifyDataSetChanged();
    }
}
