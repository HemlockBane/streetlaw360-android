package com.josholadele.streetlaw.utils;

import android.content.Context;

import com.josholadele.streetlaw.activities.BaseActivity;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.Role;

/**
 * Created by josh on 31/03/2018.
 */

public class UserUtils {

    public static boolean isJudgeOrRegistrar(Context context) {
        String s = new AppSharedPref(context).getUserCategory().toLowerCase();
        return s.equalsIgnoreCase(Role.CourtRegistrar.roleName.toLowerCase()) ||
                s.equalsIgnoreCase(Role.Judge.roleName.toLowerCase());
    }

    public static boolean isStudent(Context context) {
        String s = new AppSharedPref(context).getUserCategory().toLowerCase();
        return s.equalsIgnoreCase(Role.LawStudent.roleName.toLowerCase());
    }

    public static boolean isLawyer(Context context) {
        String s = new AppSharedPref(context).getUserCategory().toLowerCase();
        return s.equalsIgnoreCase(Role.Lawyer.roleName.toLowerCase());
    }

    public static boolean isJudge(Context context) {
        String s = new AppSharedPref(context).getUserCategory().toLowerCase();
        return s.equalsIgnoreCase(Role.Judge.roleName.toLowerCase());
    }

    public static String getInitials(String name) {
        String[] nameSplit = name.split(" ");
        if (nameSplit.length >= 2) {
            return nameSplit[0].toUpperCase() + nameSplit[1].toUpperCase();
        } else if (nameSplit.length == 1) {
            return name.substring(0, 2).toUpperCase();
        }
        return "";
    }

    public static String getCategory(Context context) {
        return new AppSharedPref(context).getUserCategory().toLowerCase();
    }

    public static boolean isRegistrar(Context context) {
        String s = new AppSharedPref(context).getUserCategory().toLowerCase();
        return s.equalsIgnoreCase(Role.CourtRegistrar.roleName.toLowerCase());
    }

    public static String getUserDesc(String category) {
        String desc = "";
        switch (category) {
            case "lawstudent":
                desc = "Law Student";
                break;
            case "cheifjudge":
                desc = "Cheif Judge";
                break;
            case "judge":
                desc = "Judge";
                break;
            case "lawyer":
                desc = "Lawyer";
                break;
            case "judicialstaff":
                desc = "Judical Staff";
                break;
            case "litigant":
                desc = "Litigant";
                break;
            default:
                desc = category;
                break;
        }
        return desc;
    }
}
