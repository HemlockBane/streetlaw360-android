package com.josholadele.streetlaw.utils;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.josholadele.streetlaw.StreetLaw360App;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Oladele on 11/28/17.
 */

public class NetworkUtils {

    public void pushToFCM(final String url, final String serverKey, final JSONObject jsonRequest, final int action, final String tag, final Response.Listener<JSONObject> volleyCallbackResponse, final Response.ErrorListener volleyErrorResponse) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    JsonObjectRequest request = new JsonObjectRequest(action, url, jsonRequest, volleyCallbackResponse, volleyErrorResponse) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> pars = new HashMap<String, String>();
                            pars.put("Content-Type", "application/json; charset=utf-8");
                            pars.put("Authorization", "key=" + serverKey);
                            return pars;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy(120000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    StreetLaw360App.getInstance().addToRequestQueue(request, tag + Calendar.getInstance().getTimeInMillis());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }
        }.execute();


    }

    public static boolean isNetworkAvailable(Context context) {
        return true;
    }
}
