package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by josh on 10/03/2018.
 */

public class Case implements Parcelable {

    public int Id;
    public String SuitNumber;
    public String Title;
    public String Description;
    public long DateRegistered;
    public int RegistrarUserId;
    //public RegistrarViewModel Registrar;
    public long DateAssigned;
    public int AssignerUserId;
    //public AssignerViewModel Assigner;
    public long StatusDate;
    public String Status;
    //public CourtDivisionViewModel CourtDivision;
    public List<User> Users;


    public Case() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }


    protected Case(Parcel in) {
//        displayName = in.readString();
//        email = in.readString();
//        phone = in.readString();
//        profileUri = in.readString();
//        facility = in.readString();
//        facilityType = in.readString();
//        facilityAddress = in.readString();
//        facilityLongtitude = in.readDouble();
//        facilityLatitude = in.readDouble();
//        facilityCity = in.readString();
//        facilityState = in.readString();
//        zone = in.readString();
//        isActivated = in.readByte() != 0;
//        speciality = in.readString();
//        role = in.readString();
    }

    public static final Creator<Case> CREATOR = new Creator<Case>() {
        @Override
        public Case createFromParcel(Parcel in) {
            return new Case(in);
        }

        @Override
        public Case[] newArray(int size) {
            return new Case[size];
        }
    };

    @Override
    public String toString() {
//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(displayName);
//        dest.writeString(email);
//        dest.writeString(phone);
//        dest.writeString(profileUri);
//        dest.writeString(facility);
//        dest.writeString(facilityAddress);
//        dest.writeDouble(facilityLongtitude);
//        dest.writeDouble(facilityLatitude);
//        dest.writeString(facilityCity);
//        dest.writeString(facilityState);
//        dest.writeString(zone);
//        dest.writeByte((byte) (isActivated ? 1 : 0));
//        dest.writeString(speciality);
//        dest.writeString(role);
    }
}