package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by josh on 10/03/2018.
 */

public class Judge implements Parcelable {

    public int Id;
    public String name;
    public String judicialDivision;
    public String username;
    public String email;

    public Judge(int id, String name, String judicialDivision, String username, String email) {
        Id = id;
        this.name = name;
        this.judicialDivision = judicialDivision;
        this.username = username;
        this.email = email;
    }

    public Judge() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }


    protected Judge(Parcel in) {
//        displayName = in.readString();
//        email = in.readString();
//        phone = in.readString();
//        profileUri = in.readString();
//        facility = in.readString();
//        facilityType = in.readString();
//        facilityAddress = in.readString();
//        facilityLongtitude = in.readDouble();
//        facilityLatitude = in.readDouble();
//        facilityCity = in.readString();
//        facilityState = in.readString();
//        zone = in.readString();
//        isActivated = in.readByte() != 0;
//        speciality = in.readString();
//        role = in.readString();
    }

    public static final Creator<Judge> CREATOR = new Creator<Judge>() {
        @Override
        public Judge createFromParcel(Parcel in) {
            return new Judge(in);
        }

        @Override
        public Judge[] newArray(int size) {
            return new Judge[size];
        }
    };

    @Override
    public String toString() {
//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(displayName);
//        dest.writeString(email);
//        dest.writeString(phone);
//        dest.writeString(profileUri);
//        dest.writeString(facility);
//        dest.writeString(facilityAddress);
//        dest.writeDouble(facilityLongtitude);
//        dest.writeDouble(facilityLatitude);
//        dest.writeString(facilityCity);
//        dest.writeString(facilityState);
//        dest.writeString(zone);
//        dest.writeByte((byte) (isActivated ? 1 : 0));
//        dest.writeString(speciality);
//        dest.writeString(role);
    }
}