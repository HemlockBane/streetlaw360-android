package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.josholadele.streetlaw.enums.CaseStatus;

import java.util.List;

/**
 * Created by josh on 10/03/2018.
 */

public class CaseFile implements Parcelable {

    public static final Creator<CaseFile> CREATOR = new Creator<CaseFile>() {
        @Override
        public CaseFile createFromParcel(Parcel in) {
            return new CaseFile(in);
        }

        @Override
        public CaseFile[] newArray(int size) {
            return new CaseFile[size];
        }
    };
    public int Id;
    public CaseStatus caseStatus;
    public String Title;
    public String suitNumber;
    public String description;
    public List<User> Users;

    public CaseFile() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }


    public CaseFile(int id, CaseStatus caseStatus, String title, String suitNumber, String description) {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
        this.Id = id;
        this.caseStatus = caseStatus;
        this.Title = title;
        this.suitNumber = suitNumber;
        this.description = description;
    }

    protected CaseFile(Parcel in) {
        this.Id = in.readInt();
        this.caseStatus = CaseStatus.valueOf(in.readString());
        this.Title = in.readString();
        this.suitNumber = in.readString();
        this.description = in.readString();
//        displayName = in.readString();
//        email = in.readString();
//        phone = in.readString();
//        profileUri = in.readString();
//        facility = in.readString();
//        facilityType = in.readString();
//        facilityAddress = in.readString();
//        facilityLongtitude = in.readDouble();
//        facilityLatitude = in.readDouble();
//        facilityCity = in.readString();
//        facilityState = in.readString();
//        zone = in.readString();
//        isActivated = in.readByte() != 0;
//        speciality = in.readString();
//        role = in.readString();
    }

    @Override
    public String toString() {
//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.caseStatus.name());
        dest.writeString(this.Title);
        dest.writeString(this.suitNumber);
        dest.writeString(this.description);
//        dest.writeString(displayName);
//        dest.writeString(email);
//        dest.writeString(phone);
//        dest.writeString(profileUri);
//        dest.writeString(facility);
//        dest.writeString(facilityAddress);
//        dest.writeDouble(facilityLongtitude);
//        dest.writeDouble(facilityLatitude);
//        dest.writeString(facilityCity);
//        dest.writeString(facilityState);
//        dest.writeString(zone);
//        dest.writeByte((byte) (isActivated ? 1 : 0));
//        dest.writeString(speciality);
//        dest.writeString(role);
    }
}