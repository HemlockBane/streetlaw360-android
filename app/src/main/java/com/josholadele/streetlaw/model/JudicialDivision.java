package com.josholadele.streetlaw.model;

public class JudicialDivision {
    public int Id;
    public String Area;
    public String State;

    public JudicialDivision(int Id, String Area, String State) {
        this.Id = Id;
        this.Area = Area;
        this.State = State;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        this.Area = area;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        this.State = state;
    }

    @Override
    public String toString(){
        return getArea();
    }

}
