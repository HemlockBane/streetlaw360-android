package com.josholadele.streetlaw.push;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.josholadele.streetlaw.MainActivity;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.PeersActivity;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.UserMessage;
import com.josholadele.streetlaw.network.NetworkHelper;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UrlConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import java.util.List;

/**
 * Created by josh on 30/03/2018.
 */

public class StreetlawMessagingService extends FirebaseMessagingService {

    private static final String TAG = StreetlawMessagingService.class.getSimpleName();

    private static final String COLLECTION_KEY = "Chat";
    private static final String DOCUMENT_USERS = "Tokens";
    private static final Query firestoreChat = FirebaseFirestore.getInstance().collection(COLLECTION_KEY);


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
// TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Map<String, String> response = remoteMessage.getData();

        // Check if message contains a data payload.
        if (response.size() > 0) {
            String message = response.get("message");
            String title = response.get("title");

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
            } else {
                // Handle message within 10 seconds
//                handleNow();
            }
            sendNotification(message);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, PeersActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.app_name);
        String CHAT_NOTIFICATION = "com.josholadele.streetlaw.CHAT_NOTIFICATION";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle("Legal Chat")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setGroup(CHAT_NOTIFICATION)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 , notificationBuilder.build());
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(token);
        AppSharedPref sharedPref = new AppSharedPref(getApplicationContext());
        sharedPref.setFCMToken(token);
        new PushRegistration(getApplicationContext()).sendTokenToServer(token);
    }

    private void sendRegistrationToServer(String token) {
        AppSharedPref sharedPref = new AppSharedPref(getApplicationContext());
        sendToServer(token, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray chatsArray = response.optJSONArray("Data");
                if (chatsArray != null) {
                    // List<UserMessage> userMessages = buildUserMessages(chatsArray);
                    //chatAdapter.setMessageList(userMessages);
                }
            }

            @Override
            public void onFailed(String reason) {

            }

            @Override
            public void onError(String reason) {

            }
        });
    }

    private void sendToServer(String token, final NetworkResponseCallback responseCallback) {
        AppSharedPref sharedPref = new AppSharedPref(getApplicationContext());
        final NetworkHelper networkHelper = new NetworkHelper();
        try {
            final JSONObject chatObject = new JSONObject();
            chatObject.put("token", token);
            chatObject.put("UserId", sharedPref.getUserId());

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.SAVE_FCM_TOKEN, "token", Request.Method.POST, chatObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }
}
