package com.josholadele.streetlaw.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.model.UserMessage;
import com.josholadele.streetlaw.modelBridge.ConversationBridge;
import com.josholadele.streetlaw.network.NetworkHelper;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UrlConstants;

import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by josh on 01/05/2018.
 */

public class ChatService extends IntentService {

    public ChatService(String name) {
        super(name);
    }

    public ChatService() {
        super("");
    }


    public final static String TYPE = "type";
    public final static String SENDER_ID = "sender-id";
    public final static int CHAT = 1;
    public final static String CHAT_CONTACT = "chat-contact";
    public final static String CONVERSATION = "conversation";
    public final static String FCM_TOKEN = "FCMToken";

    public static void pushChat(Context context, UserMessage message, ChatContact chatContact, String token) {
        saveChat(context, message, chatContact);
        context.startService(new Intent(context, ChatService.class)
                .putExtra(TYPE, CHAT)
                .putExtra(CHAT_CONTACT, chatContact)
                .putExtra(CONVERSATION, message)
                .putExtra(FCM_TOKEN, token)
        );

    }


    private static void saveChat(Context context, UserMessage conversation, ChatContact chatContact) {
        User user = new User(chatContact.name, chatContact.judicialDivision, chatContact.username, chatContact.userId, chatContact.email, conversation.time, conversation.text, chatContact.isLawyer);
        new ConversationBridge(context).saveChat(user, conversation);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent.getIntExtra(TYPE, 0) == CHAT) {
            ChatContact chatContact = intent.getParcelableExtra(CHAT_CONTACT);
            UserMessage userMessage = intent.getParcelableExtra(CONVERSATION);
            publish(chatContact, userMessage, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {

                }

                @Override
                public void onFailed(String reason) {

                }

                @Override
                public void onError(String reason) {

                }
            });
        }
    }


    private void publish(ChatContact chatContact, UserMessage userMessage, final NetworkResponseCallback responseCallback) {
        try {/*

      "Message": "Hello there",
      "SenderId": 1,
      "RecipientId": 7*/
            final NetworkHelper networkHelper = new NetworkHelper();

            final JSONObject chatObject = new JSONObject();
            chatObject.put("Message", userMessage.text);
            chatObject.put("SenderId", userMessage.userId);
            chatObject.put("RecipientId", chatContact.userId);

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.PUSH_CHAT, "chat", Request.Method.POST, chatObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();

        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public static void fetchMessages(int take, int skip, int userId, int contactId, final NetworkResponseCallback responseCallback) {

        final NetworkHelper networkHelper = new NetworkHelper();
        try {
            final JSONObject chatObject = new JSONObject();
            chatObject.put("take", take);
            chatObject.put("SenderId", userId);
            chatObject.put("RecipientId", contactId);

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_CHATS, "chats", Request.Method.POST, chatObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }
}
