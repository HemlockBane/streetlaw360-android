package com.josholadele.streetlaw;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.josholadele.streetlaw.activities.BaseActivity;
import com.josholadele.streetlaw.fragments.DiaryFragment;
import com.josholadele.streetlaw.fragments.NewsFragment;
import com.josholadele.streetlaw.fragments.NoticesFragment;

public class MainActivity extends BaseActivity {

    LinearLayout frameLayout;
    Toolbar mToolbar;
    TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        System.setProperty("https.protocols", "TLSv1.1");
        mDrawerLayout = findViewById(R.id.drawer_layout);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toggle.syncState();
        mTabLayout = findViewById(R.id.tabs);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setupNavView(navigationView);

        frameLayout = findViewById(R.id.viewpager);
        setupTabs();
    }

    private void setupTabs() {


        // Find the view pager that will allow the user to swipe between fragments
        ViewPager viewPager = findViewById(R.id.view_pager);

        // Create an adapter that knows which fragment should be shown on each page
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        mTabLayout.addTab(mTabLayout.newTab(), 0, true);
        mTabLayout.addTab(mTabLayout.newTab(), 1);
        mTabLayout.addTab(mTabLayout.newTab(), 2);

        mTabLayout.getTabAt(0).setText("Notices");
        mTabLayout.getTabAt(1).setText("News");
        mTabLayout.getTabAt(2).setText("E-Diary");

        mTabLayout.setupWithViewPager(viewPager);


        ft.replace(R.id.viewpager, new NoticesFragment());
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

        private Context mContext;

        public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            mContext = context;
        }

        // This determines the fragment for each tab
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new NoticesFragment();
            } else if (position == 1) {
                return new NewsFragment();
            } else if (position == 2) {
                return new DiaryFragment();
            } else {
                return new DiaryFragment();
            }
        }

        // This determines the number of tabs
        @Override
        public int getCount() {
            return 3;
        }

        // This determines the title for each tab
        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            switch (position) {
                case 0:
                    return mContext.getString(R.string.notices);
                case 1:
                    return mContext.getString(R.string.news);
                case 2:
                    return mContext.getString(R.string.e_diary);
                default:
                    return null;
            }
        }

    }

}
