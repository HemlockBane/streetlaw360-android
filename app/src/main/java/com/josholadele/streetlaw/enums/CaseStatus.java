package com.josholadele.streetlaw.enums;

/**
 * Created by josh on 20/03/2018.
 */

public enum CaseStatus {
    Registered(1, "Registered"),
    Assigned(2, "Assigned"),
    Transferred(3, "Transferred"),
    Adjourned(4, "Adjourned"),
    Mention(5, "Mention"),
    Hearing(6, "Hearing"),
    Continuation(7, "Continuation"),
    ReportOfService(8, "Report Of Service"),
    FurtherMention(9, "Further Mention"),
    Judgement(10, "Judgement"),
    AdoptionOfWrittenAddresses(11, "Adoption Of Written Addresses"),
    HearingOfApplication(12, "Hearing Of Application"),
    Closed(13, "Closed");

    public String statusText;
    public int statusNumber;

    CaseStatus(int i, String registered) {
        statusNumber = i;
        statusText = registered;
    }
}
