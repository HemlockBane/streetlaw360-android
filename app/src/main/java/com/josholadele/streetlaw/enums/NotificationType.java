package com.josholadele.streetlaw.enums;

/**
 * Created by josh on 20/03/2018.
 */

public enum NotificationType {
    CaseAssign(1, "CaseAssign"),
    CaseNotify(2, "CaseNotify"),
    Chat(3, "Chat");

    public String notificationText;
    public int statusNumber;

    NotificationType(int i, String notificationText) {
        statusNumber = i;
        this.notificationText = notificationText;
    }
}
