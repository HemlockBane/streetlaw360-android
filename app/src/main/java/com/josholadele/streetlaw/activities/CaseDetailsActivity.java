package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.AssigneesAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.CaseStatus;
import com.josholadele.streetlaw.model.Case;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.utils.DialogUtils;
import com.josholadele.streetlaw.utils.UserUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CaseDetailsActivity extends AppCompatActivity implements AssigneesAdapter.AssigneeClickListener {


    public static final String SELECTED_CASE_ID = "selected-case";
    TextView suitNumberTV;
    TextView suitNumber;
    TextView caseTitleTV;
    TextView caseStatusTV;
    TextView caseCurrentTV;
    TextView caseTypeTV;
    TextView caseIssueForDeterminationTV;
    TextView caseDescriptionTV;
    ScrollView pageContent;
    TextView emptyView;
    ProgressBar contentLoading;
    RecyclerView lawyersRecycler;
    CaseFile newCase;
    AppSharedPref sharedPref;
    MaterialDialog statusDialog;
    AssigneesAdapter lawyersAdapter;
    private CaseService _caseService;
    private int _selectedCaseId;
    private Case _case;
    private JSONObject _caseObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_case_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView titleText = toolbar.findViewById(R.id.toolbar_title);

        newCase = getIntent().getParcelableExtra("case");
        sharedPref = new AppSharedPref(this);

        suitNumberTV = findViewById(R.id.case_details_suitnumber_value);
        suitNumber = findViewById(R.id.case_suit_number);
        caseTitleTV = findViewById(R.id.case_title);
        caseStatusTV = findViewById(R.id.case_details_status);
        caseCurrentTV = findViewById(R.id.case_current_status);
        caseDescriptionTV = findViewById(R.id.case_description);
        caseTypeTV = findViewById(R.id.case_suit_type);
        caseIssueForDeterminationTV = findViewById(R.id.case_issue_determination);

        contentLoading = findViewById(R.id.progress_layout);
        emptyView = findViewById(R.id.empty_view);
        pageContent = findViewById(R.id.data_layout);
        lawyersRecycler = findViewById(R.id.lawyers_recyclerview);


        titleText.setText("CASE DETAILS");
        setSupportActionBar(toolbar);
//        getSupportActionBar().setHome

        _selectedCaseId = getIntent().getIntExtra(CaseDetailsActivity.SELECTED_CASE_ID, 0);
        _caseService = new CaseService();
        _case = new Case();


        caseTitleTV.setText(newCase.Title);
        caseStatusTV.setText(newCase.caseStatus.statusText);
        caseCurrentTV.setText(newCase.caseStatus.statusText);
        if (!TextUtils.isEmpty(newCase.description)) {
            caseDescriptionTV.setText(newCase.description);
        }else{

            caseDescriptionTV.setText("--");
        }
        suitNumber.setText(newCase.suitNumber);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

//        Typeface avenir = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd_Medium.otf");
        Typeface american_typewriter = Typeface.createFromAsset(getAssets(), "fonts/American_Typewriter_Regular.ttf");

        getCaseDetailsOnActivity();
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
        pageContent.setVisibility(View.GONE);

    }

    private void showDataView() {
        emptyView.setVisibility(View.GONE);
        pageContent.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        pageContent.setVisibility(View.GONE);
        contentLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (UserUtils.isJudgeOrRegistrar(this)) {
            getMenuInflater().inflate(R.menu.menu_case_detail, menu);
//        assignMenu = menu.findItem(R.id.action_assign);
//        filterMenu = menu.findItem(R.id.action_filter);
        }
        return true;
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.action_reassign) {

//            Toast.makeText(this, "Reassign", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, ChooseJudgeActivity.class);
            setIntentData(intent);
            startActivityForResult(intent, 12);
            return true;
        }
        if (id == R.id.action_change_status) {
            buildDialog();
            statusDialog.show();
            return true;
        }
        if (id == R.id.action_notify) {
            Intent intent = new Intent(this, NotifyLawyerActivity.class);

            intent.putExtra("case-id", newCase.Id);
            intent.putExtra("isReassign", false);
//            setIntentData(intent);

            startActivityForResult(intent, 102);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 102 && resultCode == Activity.RESULT_OK) {
            getCaseDetailsOnActivity();
        }
    }

    private void setIntentData(Intent intent) {
        ArrayList<Integer> selectedCaseIds = new ArrayList<>();

        selectedCaseIds.add(newCase.Id);

        if (selectedCaseIds.size() > 0) {
            intent.putExtra("selectedIds", selectedCaseIds);
        }
        intent.putExtra("isReassign", true);

    }

    private void buildDialog() {
        MaterialSimpleListAdapter adapter = new MaterialSimpleListAdapter(new MaterialSimpleListAdapter.Callback() {
            @Override
            public void onMaterialListItemSelected(MaterialDialog dialog, int index, MaterialSimpleListItem item) {

                JSONObject statusObject = new JSONObject();
                try {
                    statusObject.put("id", newCase.Id);

                    statusObject.put("status", item.getContent());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                _caseService.changeStatus(Integer.parseInt(sharedPref.getUserId()), statusObject, new NetworkResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onFailed(String reason) {

                    }

                    @Override
                    public void onError(String reason) {

                    }
                });
                statusDialog.dismiss();
            }
        });
        for (CaseStatus caseStatus : CaseStatus.values()) {
            adapter.add(new MaterialSimpleListItem.Builder(this).content(caseStatus.statusText).backgroundColor(Color.WHITE).build());

        }
        statusDialog = new MaterialDialog.Builder(this)
                .title(R.string.change_status)
                .adapter(adapter, null).build();
//        new MaterialDialog.Builder(this)
    }

    private void bindData() {
        if (_caseObject != null) {

            lawyersAdapter = new AssigneesAdapter(this);
            lawyersRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            lawyersRecycler.setAdapter(lawyersAdapter);
            JSONArray userArray = _caseObject.optJSONArray("Users");

            List<User> users = new ArrayList<>();

            for (int i = 0; i < userArray.length(); i++) {
                User user = new User();
                try {
                    JSONObject userObject = userArray.getJSONObject(i);

                    user.displayName = userObject.optString("FirstName") + " " + userObject.optString("LastName");
                    user.userId = userObject.optInt("Id");
                    users.add(user);

                    caseStatusTV.setText(userObject.optString("Status"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            lawyersAdapter.setData(users);

        }
    }

    private void getCaseDetailsOnActivity() {
        try {
            _caseService.getById(_selectedCaseId, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    _caseObject = response.optJSONObject("Data");
                    bindData();
                }

                @Override
                public void onFailed(String reason) {
                    Toast.makeText(CaseDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    Toast.makeText(CaseDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }
            });

        } catch (Exception ex) {
            Toast.makeText(this, "Unable to retrieve case: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onAssigneeClick(final User user, boolean isLongClick) {
        DialogUtils.createDialog(this, null, "Remove " + user.displayName + " from case", null, new DialogUtils.UserResponseListeners() {
            @Override
            public void onPositive() {
                _caseService.removeLawyer(user.userId, Integer.parseInt(sharedPref.getUserId()), newCase.Id, new NetworkResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        lawyersAdapter.removeLawyer(user);
                    }

                    @Override
                    public void onFailed(String reason) {
                        Toast.makeText(CaseDetailsActivity.this, "Unable to remove lawyer: " + reason, Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(String reason) {
                        Toast.makeText(CaseDetailsActivity.this, "Unable to remove lawyer: " + reason, Toast.LENGTH_SHORT).show();

                    }
                });
            }

            @Override
            public void onNegative() {

            }

            @Override
            public void onNeutral() {

            }
        });
    }
}