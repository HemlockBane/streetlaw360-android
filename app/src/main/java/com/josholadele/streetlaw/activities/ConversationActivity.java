package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.ConversationListAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.CaseStatus;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.modelBridge.ConversationBridge;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ConversationActivity extends BaseActivity implements ConversationListAdapter.ContactClickListener {

    RecyclerView caseFilesRecycler;
    ConversationListAdapter mAdapter;
    Toolbar mToolbar;

    TextView toolbarTitle;
    TextView toolbarSelectionText;

    TextView newChatTV;
    ImageView newChatImg;

    MenuItem chatMenu;
    MenuItem filterMenu;


    AppSharedPref _sharedPref;

    public static final int NEW_CHAT_REQUEST_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wrapper_conversation);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);

        _sharedPref = new AppSharedPref(this);

        toolbarTitle = mToolbar.findViewById(R.id.client_logo);
        toolbarSelectionText = mToolbar.findViewById(R.id.selection_number);

        toolbarTitle.setText("LEGAL CONVERSATIONS");
//        titleText.setText("Case Files");

        toggle.syncState();

        caseFilesRecycler = findViewById(R.id.conversation_recycler);


        newChatTV = findViewById(R.id.txt_start_new_chat);
        newChatImg = findViewById(R.id.img_start_new_chat);


        newChatTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchNewConversation();
            }
        });
        newChatImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchNewConversation();
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setupNavView(navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        setupRecyclerView(caseFilesRecycler);
        fetchContacts(sharedPref.getUserId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(contactChangeReceiver, new IntentFilter("ACTION_CONVERSATION_NOTIFICATION"));
    }

    BroadcastReceiver contactChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int userId = intent.getIntExtra("CONVERSATION_ID", -1);
            User user = intent.getParcelableExtra("EXTRA_CONVERSATION");
            mAdapter.addUser(user, true);
        }
    };

    private void launchNewConversation() {
        Intent intent = new Intent(this, NewChatActivity.class);
        startActivityForResult(intent, NEW_CHAT_REQUEST_CODE);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new ConversationListAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
//        getSampleUsers();
    }

//    @Override
//    public void onBackPressed() {
//
//        if (mAdapter.IS_MULTISELECT) {
//            mAdapter.IS_MULTISELECT = false;
//            for (int i : mAdapter.selectedPositions) {
//                caseFilesRecycler.getChildAt(i).setActivated(false);
//            }
//            mAdapter.clearSelections();
//        } else {
//            super.onBackPressed();
//        }
//    }

    void fetchContacts(String userId) {

//        showLoading();
        ConversationBridge conversationBridge = new ConversationBridge(this);
        List<User> users = conversationBridge.getAllContacts();
        mAdapter.setUsersData(users);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        chatMenu = menu.findItem(R.id.action_new_chat);
        filterMenu = menu.findItem(R.id.action_filter);
        return true;
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_new_chat) {
            Toast.makeText(this, "Start New Chat", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    List<User> files;

    @Override
    public void onContactClick(User user, boolean isLongClick) {

        Intent intent = new Intent(this, ChatScreenActivity.class);
        ChatContact selectedContact = new ChatContact(0, user.displayName, user.judicialDivision, user.userId, user.username, user.email, user.isLawyer);
        intent.putExtra("selected-contact", selectedContact);

        startActivity(intent);
    }
}
