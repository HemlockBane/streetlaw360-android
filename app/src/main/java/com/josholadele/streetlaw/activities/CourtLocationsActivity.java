package com.josholadele.streetlaw.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

//import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.Court;
import com.josholadele.streetlaw.network.CourtService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class CourtLocationsActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Toolbar mToolbar;
    TextView profileNameTV;
    TextView professionTV;

    Spinner spinnerAreas;
    Spinner spinnerStates;
    TextView shortBioTV;
    TextView addressTV;
    GoogleApiClient mGoogleApiClient;
    AppSharedPref sharedPref;
    ArrayList<String> areaList = new ArrayList<>();
    ArrayList<String> stateList = new ArrayList<String>();
    ArrayAdapter<String> adapter1;
    ArrayAdapter<String> adapter2;
    ArrayList<Court> courtLocations;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wrapper_court_locations);
        mDrawerLayout = findViewById(R.id.drawer_layout);

        sharedPref = new AppSharedPref(this);

        professionTV = findViewById(R.id.profile_profession);
        spinnerAreas = findViewById(R.id.spinnerCourts);
        spinnerStates = findViewById(R.id.spinnerCountry);
        profileNameTV = findViewById(R.id.profile_name);
        addressTV = findViewById(R.id.address);
        shortBioTV = findViewById(R.id.short_bio);

        final String[] areasArray = new String[]{"Lagos"};
        final String[] statesArray = new String[]{"Lagos"};

        // (3) create an adapter from the list
        adapter1 = new ArrayAdapter<>(
                this,
                R.layout.spinner_item_view,
                areaList
        );


        adapter2 = new ArrayAdapter<String>(
                this,
                R.layout.spinner_item_view,
                stateList
        );

        spinnerAreas.setAdapter(adapter1);
        spinnerStates.setAdapter(adapter2);

        stateList.add("Lagos");
        adapter2.notifyDataSetChanged();


        spinnerAreas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Court court = courtLocations.get(position);
                zoomToCourt(court);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerStates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String location = stateList.get(position);
                fetchCourts(location);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


//        JSONObject userObject = sharedPref.getUserObject();
//
//        String name = userObject.optString("FirstName") + " " + userObject.optString("LastName");
//
//        profileNameTV.setText(name);
//        professionTV.setText(userObject.optString("Category"));
//        shortBioTV.setText(userObject.optString("ShortBio"));
//        addressTV.setText(userObject.optString("AddressLine1"));

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);

        TextView titleText = mToolbar.findViewById(R.id.client_logo);
        titleText.setText("Court Locations");

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setupNavView(navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        fetchCourts("lagos");
        ;
//        if (mGoogleApiClient == null) {
//            mGoogleApiClient = new GoogleApiClient.Builder(this)
//                    .addConnectionCallbacks(this)
//                    .addOnConnectionFailedListener(this)
//                    .addApi(LocationServices.API)
//                    .build();
//        }

    }

    private void zoomToCourt(Court court) {

        double latitude = court.Latitude;

        // Getting longitude of the current location
        double longitude = court.Longitude;

//        float speed = location.getSpeed();

        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);

        // Showing the current location in Google Map
        CameraPosition camPos = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(15)
                .build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        mMap.animateCamera(camUpd3);
        mMap.setMaxZoomPreference(18.0f);
    }

    void getMarkerData() {

        if (courtLocations != null) {
            mMap.clear();
            areaList.clear();
            for (int i = 0; i < courtLocations.size(); i++) {
                Court court = courtLocations.get(i);
                areaList.add(court.Name);
                createMarker(court.Latitude, court.Longitude, court.Name, court.State, R.drawable.ic_check_mark_green);
            }
            adapter1.notifyDataSetChanged();
            return;
        }
        ArrayList<Court> markersArray = new ArrayList<>();
        /*
        *  public int Id;
    public String Name;
    public String City;
    public String State;
    public String Address;
    public double Latitude;
    public double Longitude;
    public String Email;
    public String Telephone;
    public int JudicialDivisionId;
        * */
        Court court1 = new Court();
        court1.Latitude = 6.5924592;
        court1.Longitude = 3.3545808;
        court1.Name = "first";
        court1.State = "Lagos";

        markersArray.add(court1);
        court1 = new Court();
        court1.Latitude = 6.5920106;
        court1.Longitude = 3.3545945;
        court1.Name = "second";
        court1.State = "Lagos";

        markersArray.add(court1);
        court1 = new Court();
        court1.Latitude = 6.5896751;
        court1.Longitude = 3.355482;
        court1.Name = "third";
        court1.State = "Lagos";


        markersArray.add(court1);


//        for (int i = 0; i < markersArray.size(); i++) {
//
//            createMarker(markersArray.get(i).Latitude, markersArray.get(i).Longitude, markersArray.get(i).Name, markersArray.get(i).State, R.drawable.ic_check_mark_green);
//        }
    }

    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng lastLocationLatLng = new LatLng(6.5136346, 3.2583069);
        mMap = googleMap;
        getMarkerData();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lastLocationLatLng));
        mMap.setMinZoomPreference(9.0f);
        mMap.setMaxZoomPreference(18.0f);
    }


    void fetchCourts(String location) {

        CourtService courtService = new CourtService();
        courtService.getCourtsByLocation(location, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                courtLocations = buildFromResult(response.optJSONArray("Data"));
                getMarkerData();
//                if (caseFileList != null && caseFileList.size() > 0) {
//                    showDataView();
//                    mAdapter.setCaseFileData(caseFileList);
//                } else {
//                    showErrorMessage();
//                }
            }

            @Override
            public void onFailed(String reason) {
                Toast.makeText(CourtLocationsActivity.this, "Failed to retrieve data", Toast.LENGTH_SHORT).show();
//                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                Toast.makeText(CourtLocationsActivity.this, "Failed to retrieve data", Toast.LENGTH_SHORT).show();
//                showErrorMessage();
            }
        });
    }

    private ArrayList<Court> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }


        ArrayList<Court> courts = new ArrayList<>();


        for (int i = 0; i < result.length(); i++) {
            JSONObject object = result.optJSONObject(i);

            Court court1 = new Court();
            court1.Latitude = object.optDouble("Latitude");
            court1.Longitude = object.optDouble("Longitude");
            court1.Name = object.optString("Name");
            court1.State = object.optString("State");

            courts.add(court1);

//            courts.add(new CaseFile(object.optInt("Id"),
//                    CaseStatus.valueOf(object.optString("Status")),
//                    object.optString("Title")));
        }
        return courts;
    }
}