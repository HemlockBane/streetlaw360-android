package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.ChatContactsAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;
import com.josholadele.streetlaw.utils.DialogUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.josholadele.streetlaw.activities.ConversationActivity.NEW_CHAT_REQUEST_CODE;

public class NewChatActivity extends AppCompatActivity implements ChatContactsAdapter.JudgeClickListener {

    RecyclerView contactsRecycler;
    ChatContactsAdapter mAdapter;
    Toolbar mToolbar;

    TextView toolbarTitle;
    TextView toolbarSelectionText;

    TextView addCaseTV;
    ImageView addCaseImg;


    ProgressBar contentLoading;
    TextView emptyView;

    MenuItem assignMenu;
    MenuItem filterMenu;

    AppSharedPref _sharedPref;
    ArrayList<Integer> selectedCaseIds;

    public static final int ADD_CASE_REQUEST_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_contact);


        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _sharedPref = new AppSharedPref(this);

        selectedCaseIds = getIntent().getIntegerArrayListExtra("selectedIds");

        toolbarTitle = mToolbar.findViewById(R.id.client_logo);
        toolbarTitle.setText("Select Lawyer/Student");
        toolbarSelectionText = mToolbar.findViewById(R.id.selection_number);


        contentLoading = findViewById(R.id.progress_layout);
        emptyView = findViewById(R.id.empty_view);

        contactsRecycler = findViewById(R.id.contact_recyclerview);

        setupRecyclerView(contactsRecycler);
//        User user =
        fetchContacts();

    }

    private void launchAddNewCase() {
        Intent intent = new Intent(this, NewCaseActivity.class);
        startActivityForResult(intent, NEW_CHAT_REQUEST_CODE);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new ChatContactsAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
//        getSampleCaseFiles();
    }

    @Override
    public void onBackPressed() {

        if (mAdapter.IS_MULTISELECT) {
            mAdapter.IS_MULTISELECT = false;
            for (int i : mAdapter.selectedPositions) {
                contactsRecycler.getChildAt(i).setActivated(false);
            }
            mAdapter.clearSelections();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_case_file, menu);
//        assignMenu = menu.findItem(R.id.action_assign);
//        filterMenu = menu.findItem(R.id.action_filter);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.action_assign) {

            return true;
        } else if (item.getItemId() == R.id.action_filter) {
            PopupMenu popupMenu = new PopupMenu(this, findViewById(R.id.action_filter));

            popupMenu.getMenuInflater().inflate(R.menu.case_filter, popupMenu.getMenu());

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    Toast.makeText(NewChatActivity.this, "Selected: " + item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            popupMenu.show();

            Toast.makeText(this, "Filter", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onJudgeClick(final ChatContact selectedContact, boolean isLongClick) {

        ChatScreenActivity.launch(this, selectedContact);
        finish();
    }

    private void assignCase(ArrayList<Integer> selectedCaseIds, int id) {
        CaseService caseService = new CaseService();
        caseService.assignCase(Integer.parseInt(new AppSharedPref(this).getUserId()), id, selectedCaseIds, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(NewChatActivity.this, "Assigned", Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
            }

            @Override
            public void onFailed(String reason) {
                Toast.makeText(NewChatActivity.this, "Unable to assign case: " + reason, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String reason) {

                Toast.makeText(NewChatActivity.this, "Unable to assign case: " + reason, Toast.LENGTH_SHORT).show();
            }
        });
    }

    List<CaseFile> files;

    void fetchContacts() {

        showLoading();
        UserService userService = new UserService();
        userService.getLawyersStudents(new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                List<ChatContact> judgeList = buildFromResult(response.optJSONArray("Data"));
                if (judgeList != null && judgeList.size() > 0) {
                    showMovieDataView();
                    mAdapter.setContactsData(judgeList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.GONE);

    }

    private void showMovieDataView() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.VISIBLE);
    }

    private List<ChatContact> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONArray movieJSONArray = null;
        try {
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<ChatContact> judges = new ArrayList<>();


        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);
            String name = object.optString("FirstName") + " " + object.optString("LastName");
            String category = object.optString("Category");
//            Judge judge = new Judge(object.optInt("Id"), name,
//                    object.optString("JudicialDivision"),
//                    object.optString("UserName"),
//                    object.optString("Email"));
            ChatContact chatContact = new ChatContact(0, name, "", object.optInt("Id"), object.optString("Username"), object.optString("Email"), category.equalsIgnoreCase("lawyer"));
            judges.add(chatContact);
        }
        return judges;
    }


    public void updateToolbar(boolean is_multiselect) {
        if (is_multiselect) {
            toolbarSelectionText.setVisibility(View.VISIBLE);
            String selectedNumer = mAdapter.selectedPositions.size() == 1 ? mAdapter.selectedPositions.size() + " case selected" : mAdapter.selectedPositions.size() + " cases selected";
            toolbarTitle.setVisibility(View.GONE);
            toolbarSelectionText.setText(selectedNumer);
            assignMenu.setVisible(true);
            filterMenu.setVisible(false);
        } else {
            toolbarSelectionText.setText("");
            toolbarSelectionText.setVisibility(View.GONE);
            toolbarTitle.setVisibility(View.VISIBLE);
            assignMenu.setVisible(false);
            filterMenu.setVisible(true);
        }
    }
}
