package com.josholadele.streetlaw.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.Case;
import com.josholadele.streetlaw.model.News;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UrlConstants;

import org.json.JSONObject;

public class NewsDetailsActivity extends AppCompatActivity {


    public static final String SELECTED_CASE_ID = "selected-case";
    TextView suitNumberTV;
    TextView caseTitleTV;
    TextView caseDetailTV;
    ImageView newsImage;
    private CaseService _caseService;
    private int _selectedCaseId;
    private Case _case;
    private JSONObject _caseObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        News news = getIntent().getParcelableExtra("news");

        if (!TextUtils.isEmpty(news.sourceUrl) && !news.sourceUrl.equalsIgnoreCase("null")) {
            setContentView(R.layout.news_web_view);
            Toolbar toolbar = findViewById(R.id.toolbar);
            TextView titleText = toolbar.findViewById(R.id.toolbar_title);


            titleText.setText(news.headline);
            setSupportActionBar(toolbar);
            ProgressBar progressBar = findViewById(R.id.progress_news);
            WebView webView = findViewById(R.id.web_view);
//            WebViewClient client = new WebViewClient();
//            client.;
            webView.loadUrl(news.sourceUrl);
//            webView.on
            webView.getSettings().setJavaScriptEnabled(true);
        } else {

            setContentView(R.layout.activity_news_detail);

            Toolbar toolbar = findViewById(R.id.toolbar);
            TextView titleText = toolbar.findViewById(R.id.toolbar_title);
            newsImage = findViewById(R.id.news_image);

            if (!TextUtils.isEmpty(news.pictureUrl) && !news.pictureUrl.equalsIgnoreCase("null")) {

//                holder.newsImageLayout.setVisibility(View.VISIBLE);
                Glide.with(this).load(UrlConstants.IMAGE_DUMP + news.pictureUrl).into(newsImage);

            } else {

//                holder.newsImageLayout.setVisibility(View.GONE);
            }

            titleText.setText("News");
            setSupportActionBar(toolbar);
            TextView headlineTextView = findViewById(R.id.txt_details_headline);
            TextView storyTextView = findViewById(R.id.txt_details_story);

            headlineTextView.setText(news.headline);
            storyTextView.setText(news.story);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

//        Typeface avenir = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd_Medium.otf");
        Typeface american_typewriter = Typeface.createFromAsset(getAssets(), "fonts/American_Typewriter_Regular.ttf");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getCaseDetailsOnActivity() {
        try {
            _caseService.getById(_selectedCaseId, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    _caseObject = response.optJSONObject("Data");
                }

                @Override
                public void onFailed(String reason) {
                    Toast.makeText(NewsDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    Toast.makeText(NewsDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }
            });

        } catch (Exception ex) {
            Toast.makeText(this, "Unable to retrieve case: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }
}