package com.josholadele.streetlaw.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.josholadele.streetlaw.LoginActivity;
import com.josholadele.streetlaw.R;

public class LandingPage extends AppCompatActivity {

    LinearLayout createAccountLayout;
    LinearLayout loginLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);

        createAccountLayout = findViewById(R.id.create_account);
        loginLayout = findViewById(R.id.login);

        createAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LandingPage.this, SignUpActivity.class));
//                finish();
            }
        });
        loginLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LandingPage.this, LoginActivity.class));
                finish();
            }
        });
    }
}
