package com.josholadele.streetlaw.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.josholadele.streetlaw.MainActivity;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.constants.RegistrationConstants;
import com.josholadele.streetlaw.customViews.NonSwipableViewPager;
import com.josholadele.streetlaw.fragments.BiodataFragment;
import com.josholadele.streetlaw.fragments.LocationFragment;
import com.josholadele.streetlaw.fragments.RoleFragment;
import com.josholadele.streetlaw.fragments.ShortBioFragment;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;
import com.josholadele.streetlaw.push.PushRegistration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.
 */
public class SignUpActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private NonSwipableViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private FragmentViewPagerAdapter newViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;


    RoleFragment roleFragment = new RoleFragment();
    BiodataFragment biodataFragment = new BiodataFragment();
    ShortBioFragment shortBioFragment = new ShortBioFragment();
    LocationFragment locationFragment = new LocationFragment();
    Fragment[] fragments;

    ProgressBar progressBar;


    Bundle regBundle;
    public JSONObject regObject = new JSONObject();
    AppSharedPref _sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        // Set up the login form.
        regBundle = new Bundle();

        fragments = new Fragment[]{roleFragment, biodataFragment, locationFragment, shortBioFragment};
        _sharedPref = new AppSharedPref(this);

        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        btnSkip = findViewById(R.id.btn_cancel);
        btnNext = findViewById(R.id.btn_next);
        progressBar = findViewById(R.id.progress_layout);


        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3,
                R.layout.welcome_slide4};


        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        addBottomDots(0);

        // making notification bar transparent
//        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        newViewPagerAdapter = new FragmentViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(newViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() != 0) {
                    int current = getItem(-1);
                    // move to next screen
                    viewPager.setCurrentItem(current);

                } else {
                    onBackPressed();
                }
//                launchHomeScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    if (viewPager.getCurrentItem() == 0) {
                        viewPager.setCurrentItem(current);
                    }
                    if (viewPager.getCurrentItem() == 1) {
                        if (biodataFragment.onNextClicked()) {
                            viewPager.setCurrentItem(current);
                        }
                    } else if (viewPager.getCurrentItem() == 2) {
                        if (locationFragment.onNextClicked()) {
                            viewPager.setCurrentItem(current);
                        }
                    } else if (viewPager.getCurrentItem() == 3) {
                        if (shortBioFragment.onNextClicked()) {
                            viewPager.setCurrentItem(current);
                        }
                    }
                } else {
                    if (shortBioFragment.onNextClicked()) {
                        signUp();
                    }
                }
            }
        });
    }

//    private void getBiodata() {
//        insertProperty(RegistrationConstants.FirstName, biodataFragment.getFirstName());
//        insertProperty(RegistrationConstants.LastName, biodataFragment.getLastName());
//        insertProperty(RegistrationConstants.Email, biodataFragment.getEmailAddress());
//        insertProperty(RegistrationConstants.UserName, biodataFragment.getUsername());
//        insertProperty(RegistrationConstants.Password, biodataFragment.getPassword());
//    }
//
//    private void getLocation() {
//        insertProperty(RegistrationConstants.FirstName, biodataFragment.getFirstName());
//        insertProperty(RegistrationConstants.LastName, biodataFragment.getLastName());
//        insertProperty(RegistrationConstants.Email, biodataFragment.getEmailAddress());
//        insertProperty(RegistrationConstants.UserName, biodataFragment.getUsername());
//    }
//
//
//    private void getShortBio() {
//        insertProperty(RegistrationConstants.FirstName, biodataFragment.getFirstName());
//        insertProperty(RegistrationConstants.LastName, biodataFragment.getLastName());
//        insertProperty(RegistrationConstants.Email, biodataFragment.getEmailAddress());
//        insertProperty(RegistrationConstants.UserName, biodataFragment.getUsername());
//    }


    public void insertProperty(String name, Object value) {
        if (regObject.has(name)) {
            regObject.remove(name);
        }
        try {
            regObject.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    UserService userService;

    private void signUp() {
//        prefManager.setFirstTimeLaunch(false);

        progressBar.setVisibility(View.VISIBLE);
        userService = new UserService();
        userService.Register(regObject, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                // progressBar.setVisibility(View.GONE);

                new PushRegistration(SignUpActivity.this).sendTokenToServer(FirebaseInstanceId.getInstance().getToken());

                new UserService().Authenticate(regObject.optString(RegistrationConstants.Email), regObject.optString(RegistrationConstants.Password), new NetworkResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
//                    showProgress(false);
                        progressBar.setVisibility(View.GONE);
                        _sharedPref.setHasLoggedIn(true);
                        _sharedPref.cacheUserObject(response.optJSONObject("Data"));
                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onFailed(String reason) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SignUpActivity.this, "Failed " + reason, Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(String reason) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SignUpActivity.this, "Error " + reason, Toast.LENGTH_SHORT).show();

                    }
                });

            }

            @Override
            public void onFailed(String reason) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(SignUpActivity.this, "Unable to register: " + reason, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String reason) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(SignUpActivity.this, reason, Toast.LENGTH_SHORT).show();

            }
        });
    }


    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }


    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.submit));
//                btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@") && email.contains(".");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(SignUpActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

//    public interface NextCallback {
//        boolean onNextPressed();
//    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    public class FragmentViewPagerAdapter extends FragmentStatePagerAdapter {

        public FragmentViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}

