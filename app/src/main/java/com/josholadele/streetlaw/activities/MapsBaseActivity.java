package com.josholadele.streetlaw.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.josholadele.streetlaw.MainActivity;
import com.josholadele.streetlaw.R;

/**
 * Created by josh on 16/03/2018.
 */

public class MapsBaseActivity extends FragmentActivity implements NavigationView.OnNavigationItemSelectedListener {
//    public DrawerLayout mDrawerLayout;
//    Toolbar mToolbar;

    public DrawerLayout mDrawerLayout;
    static int current_selection = R.id.nav_home;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    protected void setupNavView(NavigationView navigationView) {
        ImageView closeImage = navigationView.getHeaderView(0).findViewById(R.id.close_image);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawers();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        if (id == R.id.nav_home) {
            // Handle the camera action
            if (current_selection != R.id.nav_home) {
                current_selection = R.id.nav_home;
                startActivity(new Intent(this, MainActivity.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_profile) {
            // Handle the camera action
            if (current_selection != R.id.nav_profile) {
                current_selection = R.id.nav_profile;
                startActivity(new Intent(this, ProfilePage.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_case_files) {
//            fragmentTransaction.replace(R.id.viewpager, new Fraggy()).commit();
//            Toast.makeText(this, "Case Files", Toast.LENGTH_SHORT).show();
            if (current_selection != R.id.nav_case_files) {
                current_selection = R.id.nav_case_files;
                startActivity(new Intent(this, CaseFileActivity.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_court_structure) {

            Toast.makeText(this, "Court Structure", Toast.LENGTH_SHORT).show();
            if (current_selection != R.id.nav_case_files) {
                current_selection = R.id.nav_case_files;
                startActivity(new Intent(this, ProfilePage.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_legal_conversations) {
            startActivity(new Intent(this, PeersActivity.class ));
            //Toast.makeText(this, "Conversations & Contacts", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_contacts) {

            Toast.makeText(this, "Case Files", Toast.LENGTH_SHORT).show();
            if (current_selection != R.id.nav_case_files) {
                current_selection = R.id.nav_case_files;
                startActivity(new Intent(this, ProfilePage.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_court_locations) {

            Toast.makeText(this, "Court Locations", Toast.LENGTH_SHORT).show();
            if (current_selection != R.id.nav_case_files) {
                current_selection = R.id.nav_case_files;
                startActivity(new Intent(this, ProfilePage.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_settings) {

            Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_log_out) {
            startActivity(new Intent(this, LandingPage.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();

//            if (current_selection != R.id.nav_case_files) {
//                current_selection = R.id.nav_case_files;
//                startActivity(new Intent(this, ProfilePage.class));
//                finish();
////            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
//            }

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
