package com.josholadele.streetlaw.network;

/**
 * Created by josh on 10/03/2018.
 */

public interface UrlConstants {

    //String BASE_URL = "http://streetlaw.gearhostpreview.com/";
    String BASE_URL = "http://streetlaw.gear.host/";
    String URL_POST = "";



    String IMAGE_DUMP = BASE_URL + "dump/";
    String SAVE_FCM_TOKEN = BASE_URL + "api/chat/token";

    String LOGIN = BASE_URL + "api/user/authenticate";
    String SEND_TOKEN = BASE_URL + "api/user/sendtoken?email=";
    String REGISTER = BASE_URL + "api/user/register";

    String GET_JUDGES = BASE_URL + "api/users/category/judge";
    String GET_JUDGES_BY_DIVISION = BASE_URL + "api/users/judges/";
    String GET_LAWYERS_STUDENTS = BASE_URL + "api/user/lawyersandlawstudents";
    String GET_USER_CHAT_LIST = BASE_URL + "api/chat/list?id=";
    String GET_LAWYERS = BASE_URL + "api/users/category/lawyer";


    String PUSH_CHAT = BASE_URL + "api/chat/add";
    String GET_CHATS = BASE_URL + "api/chat/load";


    String SEND_RESET_TOKEN = BASE_URL + "api/user/sendtoken?email=";
    String RESET_PASSWORD = BASE_URL + "api/user/resetpassword";
    String UPLOAD_DP = BASE_URL + "api/user/setdp";
    String DOWNLOAD_DP = BASE_URL + "user/downloadprofilepic";
    String UPDATE_PROFILE_IMAGE = "http://streetlaw.gear.host/api/user/updateprofileimagename";
    String GET_USER_BY_ID = BASE_URL + "api/user/";
    String GET_NOTIFICATIONS = BASE_URL + "user/notifications/";
    String GET_LATEST_NOTIFICATION = BASE_URL + "user/notifications/";
    String UPDATE_PROFILE = BASE_URL + "api/user/update";

    String UPDATE_FCM_TOKEN = BASE_URL + "api/user/updatefcmtoken";

    String COURTS = BASE_URL + "api/courts";
    String COURTS_BY_LOCATION = BASE_URL + "api/courts/";

    String GET_CASE_BY_ID = BASE_URL + "api/case/";
    String GET_CASE_BY_USER_ID = BASE_URL + "api/user/cases";
    String ADD_CASE = BASE_URL + "api/case/register";
    String ASSIGN_CASE = BASE_URL + "api/case/assign";
    String NOTIFY_LAWYERS = BASE_URL + "api/case/notify";
    String REMOVE_LAWYER = BASE_URL + "api/case/removelawyer";
    String CHANGE_STATUS = BASE_URL + "api/case/updatestatus";

    String ADD_NOTE = BASE_URL + "api/note/add";
    String UPDATE_NOTE = BASE_URL + "api/note/update";
    String DELETE_NOTE = BASE_URL + "api/note/%s/delete";
    String GET_NOTES_BY_USER_ID = BASE_URL + "api/user/%s/notes";
    String GET_NOTE_BY_ID = BASE_URL + "api/note/";

    String GET_NOTICE_BY_ID = BASE_URL + "api/case/";
    String GET_NOTICE_BY_USER_ID = BASE_URL + "api/user/%s/notifications";


    String GET_NEWS_BY_USER_CATEGORY = BASE_URL + "api/news/";//judge";
    String GET_NEWS_BY_ID = BASE_URL + "api/user/%s/cases";

    String GET_LITIGANTS = BASE_URL + "api/users/category/litigant";
    String GET_USERS_MESSAGE_STATE = BASE_URL + "api/chat/lastreceivedorsentmessages/";


    //news
    String NEWS_BY_CATEGORY = BASE_URL + "api/news/";

}
