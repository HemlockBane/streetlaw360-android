package com.josholadele.streetlaw.network;

import android.os.AsyncTask;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by josh on 10/03/2018.
 */

public class NoticeService {

    private NetworkHelper networkHelper;

    public NoticeService() {
        networkHelper = new NetworkHelper();
    }

    public void Authenticate(String email, String password, final NetworkResponseCallback responseCallback) {
        int channel = 1;
        try {
            final JSONObject loginObject = new JSONObject();
            loginObject.put("Email", email);
            loginObject.put("Password", password);
            loginObject.put("Channel", channel);

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.LOGIN, "login", Request.Method.POST, loginObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }


    public void addCase(final JSONObject caseObject, final NetworkResponseCallback responseCallback) {
        int channel = 1;
        try {

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.ADD_CASE, "addCase", Request.Method.POST, caseObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getById(final int selectedCaseId, final NetworkResponseCallback responseCallback) {

        new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
            @Override
            protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                return networkHelper.loadFutureJsonObject(UrlConstants.GET_NOTICE_BY_ID + selectedCaseId, "getNoticeById", Request.Method.GET, null);
            }

            @Override
            protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                super.onPostExecute(response);
                if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                    responseCallback.onResponse(response.getResponse());
                } else if (response.isSuccess()) {
                    responseCallback.onFailed(response.getResponse().optString("Message"));
                } else {
                    responseCallback.onError(response.getErrorMessage());
                }
            }
        }.execute();
    }

    public void getByUser(final int userId, final NetworkResponseCallback responseCallback) {

        new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
            @Override
            protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                return networkHelper.loadFutureJsonObject(String.format(UrlConstants.GET_NOTICE_BY_USER_ID, userId), "getNotices", Request.Method.GET, null);
            }

            @Override
            protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                super.onPostExecute(response);
                if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                    responseCallback.onResponse(response.getResponse());
                } else if (response.isSuccess()) {
                    responseCallback.onFailed(response.getResponse().optString("Message"));
                } else {
                    responseCallback.onError(response.getErrorMessage());
                }
            }
        }.execute();
    }

    public void assignCase(final int userId, final int assigneeId, final ArrayList<Integer> selectedCaseIds, final NetworkResponseCallback responseCallback) {

        try {
//            int[] intArray = new int[selectedCaseIds.size()];
//            for (int i = 0; i < selectedCaseIds.size(); i++) {
//                intArray[i] = selectedCaseIds.get(i);
//            }
            final JSONObject assignCaseObject = new JSONObject();
            assignCaseObject.put("CaseIds", new JSONArray(selectedCaseIds));
            assignCaseObject.put("AssignerUserId", userId);
            assignCaseObject.put("AssigneeUserId", assigneeId);


            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.ASSIGN_CASE, "assignCase", Request.Method.POST, assignCaseObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }

    }


//    public async Task<DataResult<UserViewModel>>
//
//    Register(SignUpViewModel signUpViewModel) {
//
//        DataResult<UserViewModel> result = null;
//        try {
//            var jsonRequest = JsonConvert.SerializeObject(signUpViewModel, Formatting.Indented);
//            var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "text/json");
//
//            var response = _client.PostAsync("api/user/register", httpContent).Result;
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < UserViewModel >> (content);
//
//            var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        } catch (Exception ex) {
//            //_logger.Error(ex.Message);
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = null;
//        }
//
//        return result;
//    }
//
//    public async Task<DataResult<UserViewModel>>
//
//    GetById(int id) {
//
//        DataResult<UserViewModel> result = null;
//        try {
//            var response = _client.GetAsync($"user/{id}").Result;
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < UserViewModel >> (content);
//
//            var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        } catch (Exception ex) {
//            //_logger.Error(ex.Message);
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = null;
//        }
//
//        return result;
//    }
//
//
//    public async Task<DataResult<UserViewModel>>
//
//    UpdateProfile(UserViewModel userViewModel) {
//
//        DataResult<UserViewModel> result = null;
//        try {
//            var jsonRequest = JsonConvert.SerializeObject(userViewModel, Formatting.Indented);
//            var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "text/json");
//
//            var response = _client.PutAsync("user/update", httpContent).Result;
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < UserViewModel >> (content);
//
//            var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        } catch (Exception ex) {
//            //_logger.Error(ex.Message);
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = null;
//        }
//
//        return result;
//    }
//
//    public async Task<DataResult<UserViewModel>>
//
//    GetUserWithNotifications(int userid) {
//        var cache = BlobCache.LocalMachine;
//
//        var cacheduserNotifications = cache.GetAndFetchLatest("notifications", async() = > await
//        GetRemoteUserWithNotificationsAsync(userid),
//                offset =>
//        {
//            TimeSpan elapsed = DateTimeOffset.Now - offset;
//            return elapsed > new TimeSpan(hours:0, minutes:30, seconds:0);
//        });
//
//        var notifications = await cacheduserNotifications.FirstOrDefaultAsync();
//        return notifications;
//    }
//
//    private async Task<DataResult<UserViewModel>>
//
//    GetRemoteUserWithNotificationsAsync(int userid) {
//        DataResult<UserViewModel> result = null;
//        try {
//
//            var response = await _client.GetAsync($"user/notifications/{userid}");
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < UserViewModel >> (content);
//
//        } catch (Exception ex) {
//            //log exception
//            // client Side Error occured
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = null;
//
//        }
//        return result;
//    }
//
//
//    public async Task<DataResult<NotificationViewModel>>
//
//    GetLatestNotificationByUserIdAsync(int userid) {
//
//        DataResult<NotificationViewModel> result = null;
//        try {
//            //var jsonRequest = JsonConvert.SerializeObject(signUpViewModel, Formatting.Indented);
//            //var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "text/json");
//
//            var response = _client.GetAsync($"user/notification/latest/{userid}").Result;
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < NotificationViewModel >> (content);
//
//            var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        } catch (Exception ex) {
//            //_logger.Error(ex.Message);
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = null;
//        }
//
//        return result;
//    }
//
//    public async Task<DataResult<Boolean>>
//
//    RequestPasswordResetTokenAsync(string email) {
//
//        DataResult<Boolean> result = null;
//        try {
//            //var requestTokenVM = new RequestTokenViewModel { email = email };
//
//            //var jsonRequest = JsonConvert.SerializeObject(requestTokenVM);
//
//            //var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "text/json");
//            //var response = _client.GetAsync($"user/{id}").Result;
//
//            var response = await _client.GetAsync($"user/sendtoken?email={email}");
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < Boolean >> (content);
//
//            var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        } catch (Exception ex) {
//            //_logger.Error(ex.Message);
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = false;
//        }
//
//        return result;
//    }
//
//    public async Task<DataResult<Boolean>>
//
//    ResetPasswordAsync(PasswordResetViewModel passwordResetViewModel) {
//
//        DataResult<Boolean> result = null;
//        try {
//            var jsonRequest = JsonConvert.SerializeObject(passwordResetViewModel);
//            var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "text/json");
//
//            var response = await _client.PutAsync("user/resetpassword", httpContent);
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < Boolean >> (content);
//
//            var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        } catch (Exception ex) {
//            //_logger.Error(ex.Message);
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = false;
//        }
//
//        return result;
//    }
//
//    public async Task<DataResult<bool>>
//
//    UploadProfilePicture(ProfilePictureRequestViewModel profilePictureRequestViewModel) {
//        DataResult<Boolean> result = null;
//        try {
//            var jsonRequest = JsonConvert.SerializeObject(profilePictureRequestViewModel);
//            var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
//
//            var response = _client.PostAsync("api/user/setdp", httpContent).Result;
//
//            response.EnsureSuccessStatusCode();
//
//            var content = await response.Content.ReadAsStringAsync();
//            result = JsonConvert.DeserializeObject < DataResult < Boolean >> (content);
//
//            var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        } catch (Exception ex) {
//            //_logger.Error(ex.Message);
//            result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//            result.Message = ApplicationConstants.clientErrorMessage;
//            result.Data = false;
//        }
//
//        return result;
//    }
//
//    public async Task<byte[]>
//
//    DownloadProfilePic(string filename) {
//        DataResult<byte[]> result = null;
//        //try
//        //{
//        var response = await _client.GetByteArrayAsync("user/downloadprofilepic" + filename);
//
//        //response.EnsureSuccessStatusCode();
//
//        //var content = await response.Content.ReadAsStringAsync();
//        //result = JsonConvert.DeserializeObject<DataResult<byte[]>>(content);
//
//        var responses = ApiResponseFormat.GetApiResponseMessages();
//
//        //}
//        //catch (Exception ex)
//        //{
//        //    //_logger.Error(ex.Message);
//        //    //result.StatusCode = ApplicationConstants.clientErrorStatusCode;
//        //    //result.Message = ApplicationConstants.clientErrorMessage;
//        //    //result.Data = null;
//        //}
//
//        return response;
//    }
}
